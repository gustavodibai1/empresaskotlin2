package com.example.empresakotlin2.view.ui.main

enum class EnterpriseViewState {
    LOADING, SUCCESS, ERROR, NETWORK_ERROR
}