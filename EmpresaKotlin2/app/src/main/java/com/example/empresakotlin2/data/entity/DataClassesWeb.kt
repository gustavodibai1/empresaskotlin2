package com.example.empresakotlin2.data.entity

import com.google.gson.annotations.SerializedName

data class ConfigurationModel(
    @SerializedName("accessToken") var accessToken : String = "",
    @SerializedName("client") var client : String = "",
    @SerializedName("uid") var uid : String = ""
)

data class Enterprise(
    @SerializedName("id") val id : Long = 0L,
    @SerializedName("enterprise_name") val enterpriseName : String,
    @SerializedName("description") val description : String,
    @SerializedName("photo") val photo : String,
    @SerializedName("country") val country : String,
    @SerializedName("enterprise_type") val enterpriseType : EnterpriseTypeModel
)

data class EnterpriseTypeModel (
    @SerializedName("id") val id : Long = 0L,
    @SerializedName("enterprise_type_name") val typeName : String = ""
)

data class ResponseEnterprise(
    val enterprises: MutableList<Enterprise>
)

data class User(
    @SerializedName("email") val email : String = "",
    @SerializedName("password") val password : String = ""
)