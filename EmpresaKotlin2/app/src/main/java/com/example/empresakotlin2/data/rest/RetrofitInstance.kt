package com.example.empresakotlin2.data.rest

import com.example.empresakotlin2.data.entity.ResponseEnterprise
import com.example.empresakotlin2.data.entity.User
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

public class RetrofitInstance {

    val BASE_URL: String = "http://empresas.ioasys.com.br/api/v1/"

    fun getAPI(): AccessEndPoints {
        val gson = GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logInterceptor)

        val retrofit = Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client.build())
            .build()

        return retrofit
            .create<AccessEndPoints>(AccessEndPoints::class.java)
    }

    public interface AccessEndPoints {

        @POST("users/auth/sign_in")
        @Headers("Content-Type: application/json")
        fun loginUsuario(@Body usuario: User): Observable<Response<User>>

        @GET(
            "enterprises/")
        fun getEmpresas(
            @Header("access-token") token: String,
            @Header("client") client: String,
            @Header("uid") uid: String
        ): Observable<ResponseEnterprise>

        @GET("enterprises/id")
        fun show(@Path("id") id: Int): Observable<ResponseEnterprise>
    }

}