package com.example.empresakotlin2.util

import android.content.Context
import com.example.empresakotlin2.data.entity.ConfigurationModel
import com.google.gson.Gson

public class SharedPreferencesHelper {
    private val context : Context


    constructor(context: Context) {
        this.context = context
    }

    fun saveConfigurationModel(configurationModel: ConfigurationModel) {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        val editor = sharedPreferences.edit()
        editor.putString("configurationModel", Gson().toJson(configurationModel))
        editor.apply()
    }

    fun getConfigurationModel() : ConfigurationModel {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        return Gson().fromJson(sharedPreferences.getString("configurationModel", ""), ConfigurationModel::class.java)
    }

    fun saveToSharedPreferences(token : String, client : String, uid : String) {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        val editor = sharedPreferences.edit()
        editor.putString("TOKEN", token)
        editor.putString("CLIENT", client)
        editor.putString("UID", uid)
        editor.apply()
    }

    fun getToken() : String {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        return sharedPreferences.getString("TOKEN", "")
    }

    fun getClient() : String {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        return sharedPreferences.getString("CLIENT", "")
    }

    fun getUID() : String {
        val sharedPreferences = context.getSharedPreferences("accessData", 0)
        return sharedPreferences.getString("UID", "")
    }
}