package com.example.empresasandroidkotlin.data.repository

import com.example.empresakotlin2.data.Result
import com.example.empresakotlin2.data.entity.ConfigurationModel
import com.example.empresakotlin2.data.entity.Enterprise
import com.example.empresakotlin2.data.rest.RetrofitInstance
import com.example.empresakotlin2.util.SharedPreferencesHelper
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class EnterpriseRepository {

    fun mostrarEmpresas(helper: SharedPreferencesHelper)
            : Observable<MutableList<Enterprise>> {

        lateinit var observer : Observer<MutableList<Enterprise>>
        var result = Observable.unsafeCreate<MutableList<Enterprise>> {
            observer = it as Observer<MutableList<Enterprise>>
        }

        val configurationModel = helper.getConfigurationModel()
        val api = RetrofitInstance()

        api.getAPI().getEmpresas(
            configurationModel.accessToken,
            configurationModel.client,
            configurationModel.uid
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                response ->
                val enterpriseList = response.enterprises
                observer.onNext(enterpriseList)
            }, {
                e ->
                observer.onError(e)
            })

        return result
    }

}