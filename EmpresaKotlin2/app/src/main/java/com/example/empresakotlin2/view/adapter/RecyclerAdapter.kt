package com.example.empresakotlin2.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.empresakotlin2.R
import com.example.empresakotlin2.data.entity.Enterprise

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>, Filterable {

    var context : Context
    lateinit var empresa : Enterprise
    private var enterpriseListTemp : Boolean = false
    var enterpriseList : MutableList<Enterprise> = mutableListOf()
    var enterpriseListFull = mutableListOf<Enterprise>()

//    private var clickListener : EnterpriseClickListener? = null

    constructor(context: Context) : super() {
        this.context = context
    }

    fun setAdapter(enterprises : MutableList<Enterprise>){
        this.enterpriseList = enterprises
        enterpriseList.map { enterpriseListFull.add(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_layout,
            parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return enterpriseList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterprise : Enterprise = enterpriseList.get(position)

        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(context)
            .load(context.getString(R.string.BASE_URL) + enterpriseList.get(position).photo)
            .fitCenter()
            .placeholder(circularProgressDrawable)
            .error(R.drawable.img_e_1_lista)
            .into(holder.img)

        holder.txtName.text = enterprise.enterpriseName
        holder.txtCountry.text = enterprise.country
        holder.txtType.text = enterprise.enterpriseType.typeName
    }

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                if (enterpriseListTemp) {
                    enterpriseList = enterpriseListFull
                    notifyDataSetChanged()
                    enterpriseListTemp = false
                } else {
                    enterpriseList = results.values as ArrayList<Enterprise>
                    notifyDataSetChanged()
                }
            }

            override fun performFiltering(constraint: CharSequence): FilterResults {
                var constraint = constraint
                val results = FilterResults()
                val FilteredArrayNames = java.util.ArrayList<Enterprise>()

                constraint = constraint.toString().toLowerCase()

                if (constraint != "") {
                    for (i in enterpriseListFull.indices) {
                        val dataNames = enterpriseListFull[i]
                        if (dataNames.enterpriseName.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrayNames.add(dataNames)
                        }
                    }

                    results.count = FilteredArrayNames.size
                    results.values = FilteredArrayNames
                    Log.e("VALUES", results.values.toString())
                    return results
                } else {
                    enterpriseListTemp = true
                    return results
                }
            }
        }
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)
//        , View.OnClickListener
    {

        var parentLayout : ConstraintLayout

        var img : ImageView
        var txtName : TextView
        var txtCountry : TextView
        var txtType : TextView

        init {
            parentLayout = itemView.findViewById(R.id.parent_layout)
//            parentLayout.setOnClickListener(this)

            img = itemView.findViewById(R.id.imgLogo)
            txtName = itemView.findViewById(R.id.txtTitle)
            txtCountry = itemView.findViewById(R.id.txtCountry)
            txtType = itemView.findViewById(R.id.txtType)
        }

//        override fun onClick(v: View?) {
//            if(clickListener != null)
//                clickListener!!.clickItem(v, enterpriseList.get(adapterPosition))
//        }
    }

//    fun setClickListener(clickListener: EnterpriseClickListener) {
//        this.clickListener = clickListener
//    }
}