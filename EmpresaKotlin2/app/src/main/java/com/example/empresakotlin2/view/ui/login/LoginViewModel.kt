package com.example.empresakotlin2.view.ui.login


import androidx.lifecycle.*
import com.example.empresakotlin2.data.entity.ConfigurationModel
import com.example.empresakotlin2.data.entity.User
import com.example.empresakotlin2.data.repository.UserRepository
import com.example.empresakotlin2.view.ViewState
import  com.example.empresakotlin2.view.ui.login.LoginViewState.*
import  com.example.empresakotlin2.data.Result
import io.reactivex.Observable
import io.reactivex.Observer


class LoginViewModel() : ViewModel() {

    private val loginState: MutableLiveData<ViewState<ConfigurationModel, LoginViewState>> = MutableLiveData()
    private lateinit var repo: UserRepository

    constructor(repo: UserRepository) : this() {
        this.repo = repo
    }

    fun observeLoginState(): MutableLiveData<ViewState<ConfigurationModel, LoginViewState>> = loginState

    fun fazerLogin(user: User) {

        if (isValidUserCredentials(user)) {
            loginState.postValue(ViewState(LOADING))
            val result = repo.logar(user)
                .subscribe({
                    loginState.postValue(
                        ViewState(
                            SUCCESS,
                            it
                        )
                    )
                }, {
                    loginState.postValue(
                        ViewState(
                            ERROR,
                            error = it
                        )
                    )
                })
        }
    }

    private fun isValidUserCredentials(user: User): Boolean {
        if (user.email.isEmpty()) {
            loginState.postValue(ViewState(EMPTY_EMAIL))
            return false
        }
        if (user.password.isEmpty()) {
            loginState.postValue(ViewState(EMPTY_PASSWORD))
            return false
        }

        return true
    }
}