package com.example.empresakotlin2.view.ui.main

import  com.example.empresakotlin2.view.ui.main.EnterpriseViewState.*

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.empresakotlin2.R
import com.example.empresakotlin2.data.entity.Enterprise
import com.example.empresakotlin2.util.SharedPreferencesHelper
import com.example.empresakotlin2.view.adapter.RecyclerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(){

    lateinit var adapter : RecyclerAdapter
    lateinit var helper : SharedPreferencesHelper

    val viewModel : EnterpriseViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayShowTitleEnabled(false)

        adapter = RecyclerAdapter(this)
        helper = SharedPreferencesHelper(this)

        progress_bar.visibility = View.GONE

        recyclerView.visibility = View.INVISIBLE

        val linearLayoutManager : LinearLayoutManager? = LinearLayoutManager(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = linearLayoutManager

        viewModel.mostrarEmpresas(helper)
    }

    override fun onResume() {
        super.onResume()

        if(!viewModel.observeEnterpriseState().hasObservers()) {
            viewModel.observeEnterpriseState().observe(this, Observer {
                when (it.status) {
                    LOADING -> progress_bar.visibility = View.VISIBLE
                    SUCCESS -> onSuccessLoading(it.data)
                    ERROR -> onError(it.error)
                    NETWORK_ERROR -> onNetworkError()
                }
            })
        }
    }

    private fun onNetworkError() {
        Toast.makeText(this@MainActivity, "Error de conexão!", Toast.LENGTH_SHORT).show()
    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: MutableList<Enterprise>?) {
        adapter.setAdapter(data!!)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)

        val searchItem = menu.findItem(R.id.action_search)

        val mSearchView = searchItem.actionView as SearchView
        mSearchView.queryHint = getString(R.string.hintSearchViewHome)

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                txtHome.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                txtHome.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
                this@MainActivity.adapter.filter.filter(newText)
                return true
            }
        })

        return true
    }

}
