package com.example.empresakotlin2.view.ui.login

import  com.example.empresakotlin2.view.ui.login.LoginViewState.*

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.example.empresakotlin2.R
import com.example.empresakotlin2.data.entity.ConfigurationModel
import com.example.empresakotlin2.data.entity.User
import com.example.empresakotlin2.util.Constants
import com.example.empresakotlin2.view.ui.main.MainActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : FragmentActivity() {

    val viewModel : LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progress_bar.visibility = View.INVISIBLE

        editEmail.setText("testeapple@ioasys.com.br")
        editSenha.setText("12341234")
        login()
    }

    override fun onResume() {
        super.onResume()
        if (!viewModel.observeLoginState().hasObservers()) {
            viewModel.observeLoginState().observe(this, Observer {
                when (it.status) {
                    LOADING -> {
                        //Aqui vai ser quando estiver sendo carregado
                        progress_bar.visibility = View.VISIBLE
                    }
                    SUCCESS -> onSuccessLogin(it.data)
                    ERROR -> onErrorLogin(it.error)
                    EMPTY_EMAIL -> onEmptyEmail()
                    EMPTY_PASSWORD -> onEmptyPassword()
                    NETWORK_ERROR -> onNetworkError()
                }
            })
        }
    }

    private fun onNetworkError() {
        Toast.makeText(this@LoginActivity, getString(R.string.error_network), Toast.LENGTH_SHORT).show()
    }

    private fun onEmptyPassword() {
        Toast.makeText(this@LoginActivity, getString(R.string.error_empty_password), Toast.LENGTH_SHORT).show()

    }

    private fun onEmptyEmail() {
        Toast.makeText(this@LoginActivity, getString(R.string.error_empty_email), Toast.LENGTH_SHORT).show()
    }

    private fun onErrorLogin(error: Throwable?) {
        Log.e(TAG, error.toString())
        progress_bar.visibility = View.GONE
    }

    private fun onSuccessLogin(data: ConfigurationModel?) {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        intent.putExtra(Constants.EXTRA_CONFIGURATION_MODEL, Gson().toJson(data))
        this@LoginActivity.startActivity(intent)
        progress_bar.visibility = View.GONE
    }

    fun login() {
        btnEntrar.setOnClickListener {
            val email = editEmail!!.text.toString()
            val senha = editSenha!!.text.toString()

            viewModel.fazerLogin(User(email, senha))
        }
    }

    companion object {
        val TAG: String = "LoginActivity"
    }
}