package com.example.empresakotlin2.view.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.empresakotlin2.data.entity.Enterprise
import com.example.empresakotlin2.view.ViewState
import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository
import  com.example.empresakotlin2.data.Result
import com.example.empresakotlin2.util.SharedPreferencesHelper


class EnterpriseViewModel(val repo: EnterpriseRepository) : ViewModel() {

    val enterpriseState: MutableLiveData<ViewState<MutableList<Enterprise>, EnterpriseViewState>> = MutableLiveData()

    fun observeEnterpriseState(): MutableLiveData<ViewState<MutableList<Enterprise>, EnterpriseViewState>> =
        enterpriseState

    fun mostrarEmpresas(
        helper: SharedPreferencesHelper
    ) {
        val result = repo.mostrarEmpresas(helper)
            .subscribe({
                enterpriseState.postValue(
                    ViewState(
                        EnterpriseViewState.SUCCESS,
                        it
                    )
                )
            }, {
                enterpriseState.postValue(
                    ViewState(
                        EnterpriseViewState.ERROR,
                        error = it
                    )
                )
            })
    }
}