package com.example.empresakotlin2.data.repository

import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.util.Log
import com.example.empresakotlin2.data.entity.ConfigurationModel
import com.example.empresakotlin2.data.entity.User
import com.example.empresakotlin2.data.rest.RetrofitInstance
import com.example.empresakotlin2.data.Result
import com.example.empresakotlin2.util.ConnectivityHelper
import com.example.empresakotlin2.util.SharedPreferencesHelper
import io.reactivex.Observable
import io.reactivex.Observable.unsafeCreate
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscriber

class UserRepository(
    private val helper: SharedPreferencesHelper,
    private val connectivityHelper: ConnectivityHelper
) {

    @SuppressLint("CheckResult")
    fun logar(user: User)
            : Observable<ConfigurationModel> {

        lateinit var observer : Observer<ConfigurationModel>
        var result = Observable.unsafeCreate<ConfigurationModel> {
            observer = it as Observer<ConfigurationModel>
        }

        if (connectivityHelper.verificaConexao()) {
            observer.onError(NetworkErrorException())
        } else {
            val configurationModel = ConfigurationModel()
            val api = RetrofitInstance()

            api.getAPI().loginUsuario(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    configurationModel.accessToken = response.headers().get("access-token")!!
                    configurationModel.client = response.headers().get("client")!!
                    configurationModel.uid = response.headers().get("uid")!!

                    helper.saveConfigurationModel(configurationModel)

                    observer.onNext(configurationModel)
                }, { e ->
                    observer.onError(e)
                }, {
                    Log.d("UserRepository", "onCompleted")
                })
        }

        return result
    }
}