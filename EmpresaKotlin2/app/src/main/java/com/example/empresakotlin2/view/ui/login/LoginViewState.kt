package com.example.empresakotlin2.view.ui.login

enum class LoginViewState {
    LOADING, SUCCESS, ERROR, INVALID_EMAIL, EMPTY_EMAIL, EMPTY_PASSWORD, NETWORK_ERROR
}