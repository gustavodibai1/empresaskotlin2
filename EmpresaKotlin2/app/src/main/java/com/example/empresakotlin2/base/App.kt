package com.example.empresakotlin2.base

import android.app.Application
import com.example.empresakotlin2.modules.androidUtilsModule
import com.example.empresakotlin2.modules.repositoryModule
import com.example.empresakotlin2.modules.viewModelModule
import org.koin.android.ext.android.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(viewModelModule, repositoryModule, androidUtilsModule))
    }
}