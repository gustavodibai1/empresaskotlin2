package com.example.empresakotlin2.modules

import com.example.empresakotlin2.data.repository.UserRepository
import com.example.empresakotlin2.util.ConnectivityHelper
import com.example.empresakotlin2.util.SharedPreferencesHelper
import com.example.empresakotlin2.view.ui.login.LoginViewModel
import com.example.empresakotlin2.view.ui.main.EnterpriseViewModel
import com.example.empresasandroidkotlin.data.repository.EnterpriseRepository

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


val androidUtilsModule = module {

    single {
        SharedPreferencesHelper(get())
    }

    single {
        ConnectivityHelper(get())
    }

}

val repositoryModule = module {

    factory { UserRepository(get(), get()) }

    factory { EnterpriseRepository() }

}

val viewModelModule = module {

    viewModel {
        LoginViewModel(get())
    }

    viewModel {
        EnterpriseViewModel(get())
    }

}